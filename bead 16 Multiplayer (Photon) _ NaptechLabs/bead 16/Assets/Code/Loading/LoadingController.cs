﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoadingController : MonoBehaviour
{
    [Header("Please input Text without any DOT like - (.)")]
    public TextMeshProUGUI _loadingText;
    public float _loadingAnimationSpeed;

    private string _dot;
    public string _textStore;
    public string _textStoreBackup;
    // Start is called before the first frame update
    void Start()
    {
         _textStoreBackup = _loadingText.text;
        if(_loadingText.text != null){
            _textStore = _loadingText.text;
            StartCoroutine(DOTAnimaton());
        }
    }

    // Update is called once per frame
    void Update()
    {
        _loadingText.text = _textStore;
    }

    IEnumerator DOTAnimaton(){
        _dot = "";
        _textStore = _textStoreBackup;
        yield return new WaitForSeconds(_loadingAnimationSpeed);
        _dot += ".";
        _textStore += _dot;
        yield return new WaitForSeconds(_loadingAnimationSpeed);
        _dot += ".";
        _textStore += _dot;
        yield return new WaitForSeconds(_loadingAnimationSpeed);
        _dot += ".";
        _textStore += _dot;
        yield return new WaitForSeconds(_loadingAnimationSpeed);
        _dot += ".";
        _textStore += _dot;
        yield return new WaitForSeconds(_loadingAnimationSpeed);
        StartCoroutine(DOTAnimaton());
    }
}
