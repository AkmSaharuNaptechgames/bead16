﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SGDSOFT;
public class BeadPosition : MonoBehaviour
{
    public static BeadPosition _BeadPosition {get;set;}

    public bool _isNextHit;
    [Header("Bead Hit Position Permission")]
    public bool _up,down,left,right,up_left,up_right,down_left,down_right;

   [Header("PhotonMultiplayer")]
    public bool _isPhotonMultiplayer;

    [Header("Don't Touch")]
    public GameObject _storePlayer;
    public bool _isMove;
    public GameObject _destoryingObject;
    public bool _isOneLoop_A;
    
    
    // Start is called before the first frame update
    void Awake()
    {
        _BeadPosition = this;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayerTransformRaycastMouseHit(GameObject _player,GameObject _destoryObj)
    {
        if(!_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled){
            _storePlayer = _player;
            _destoryingObject = _destoryObj;

            if (_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
            {
                if(GameManager._gameManager._clientTime_ForMultiplayer <= 0 || GameManager._gameManager._playerIndivisualTimeIndex <= 0)
                {    if (_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>().enabled == true)
                    {
                        _player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = true;       
                       
                    }   
                }
            }
          //  _isOneLoop_A = false;
        }else{ 

            if (_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isMove == false){
                
                _storePlayer = _player;
                _destoryingObject = _destoryObj;

              //  if(_isOneLoop_A == false){
                    _player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._RayHitList.Add(_player);
                //   _isOneLoop_A = true;
                //  }

                //ByPass
                if (_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
                {
                    if(GameManager._gameManager._clientTime_ForMultiplayer <= 0 || GameManager._gameManager._playerIndivisualTimeIndex <= 0)
                    {
                        if(_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>().enabled == true)
                        {
                            if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled)
                            {
                                if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isOneLoop_B == false)
                                {
                                   // _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isRayHit = false;
                                    //_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isOneLoop_A = false;
                                   // _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isOneLoop_B = true;
                                }
                            }
                        }
                    }
                }
                

                /* if(_isOneLoop_A == true){
                     if(_player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._RayHitList.Count == 0){
                         if(GameManager._gameManager._playerIndivisualTimeIndex < 25f){
                             _isOneLoop_A = false;
                         }
                     }
                 }*/
                //IsMove off
                if (_player.GetComponent<BeadGutiAISystem>()._readyForAIMove == true){
                    _player.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isMove = true;
                    
                    Invoke("TimeToMove",1.5f);
                }
            }

        }

    }

    void TimeToMove(){

        try
        {
            _storePlayer.GetComponent<BeadGutiAISystem>()._readyForAIMove = false;
            _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._RayHitList.Clear();
        }
        catch
        {
            return;
        }

        if (_storePlayer != null){

                _storePlayer.transform.position = transform.position;
                
                if(_destoryingObject != null)
                {
                    GameManager._gameManager._playerIndivisualTimeIndex = 40;
                    Instantiate(GameManager._gameManager._destoryEffect,_destoryingObject.transform.position,Quaternion.identity);
                    KillUpdate(_storePlayer.GetComponent<BeadGutiAISystem>()._userToken);
                    _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isMove = false;
                    if (_isPhotonMultiplayer == false)
                    {
                        Destroy(_destoryingObject);
                    }
                    else
                    {
                        if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>().enabled == true)
                        {
                            _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = false;
                        }
                        PlayerNetworking_SGDSOFT _network = _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>();
                        _network.NetworkPlayerDestory(_destoryingObject.GetComponent<PhotonView>().viewID, _storePlayer.GetComponent<BeadGutiAISystem>()._userToken);
                    }

                    //VibrateControll
                    VibrateControll();

                }else{

                    if (_isPhotonMultiplayer == false)
                    {
                        GameManager._gameManager._playerIndivisualTimeIndex = 40;
                        GameManager._gameManager.SwithPlayerManagerButton();
                        _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isMove = false;
                    }
                    else
                    {
                        PlayerNetworking_SGDSOFT _network = _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>();
                        _network.TurnControll();
                    }
                }
                
                for(int i = 0; i < BeadBoardManager._BeadBoardManager._childs.Length; i++){
                    if(BeadBoardManager._BeadBoardManager._childs[i].gameObject.name != this.gameObject.name){
                        BeadBoardManager._BeadBoardManager._childs[i].GetComponent<BeadPosition>()._storePlayer = null;
                    }
                }
                if( _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled){
                    _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isRayHit = false;
                    _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>()._isOneLoop_A = false;
                }

                if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
                {
                    if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>().enabled == true)
                    {
                        _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = false;
                    }
                }
            }
    }
    

    private void OnMouseDown()
    {
        if (GameManager._gameManager._isPhotonMultiplayer == false)
        {
            if (GameManager._gameManager._isRobotTurn == false)
            {
                if (_storePlayer != null)
                {
                    //if(_storePlayer.GetComponent<BeadGutiAISystem>()._isAccessForBead == true){
                    _storePlayer.transform.position = transform.position;
                   // Vector3 _smoothPos = _storePlayer.transform.position;
                   // _storePlayer.transform.position = Vector3.Lerp(_smoothPos, transform.position, 2f);
                    //  _storePlayer.GetComponent<BeadGutiAISystem>()._isAccessForBead = false;
                    //  }
                    if (_destoryingObject != null)
                    {

                        if (_isPhotonMultiplayer == false)
                        {
                            GameManager._gameManager._playerIndivisualTimeIndex = 40;
                            Instantiate(GameManager._gameManager._destoryEffect, _destoryingObject.transform.position, Quaternion.identity);
                            KillUpdate(_storePlayer.GetComponent<BeadGutiAISystem>()._userToken);
                            Destroy(_destoryingObject);

                            //VibrateControll
                            VibrateControll();

                        }
                        else
                        {
                            PlayerNetworking_SGDSOFT _network = _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>();
                            _network.NetworkPlayerDestory(_destoryingObject.GetComponent<PhotonView>().viewID, _storePlayer.GetComponent<BeadGutiAISystem>()._userToken);
                        }

                    }
                    else
                    {
                        if (_isPhotonMultiplayer == false)
                        {
                            GameManager._gameManager._playerIndivisualTimeIndex = 40;
                            GameManager._gameManager.SwithPlayerManagerButton();
                        }
                        else
                        {
                            PlayerNetworking_SGDSOFT _network = _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>();
                            _network.TurnControll();
                        }
                    }



                }
                else
                {
                    return;
                }

                for (int i = 0; i < BeadBoardManager._BeadBoardManager._childs.Length; i++)
                {
                    if (BeadBoardManager._BeadBoardManager._childs[i].gameObject.name != this.gameObject.name)
                    {
                        BeadBoardManager._BeadBoardManager._childs[i].GetComponent<BeadPosition>()._storePlayer = null;
                    }
                }
            }
        }
        else
        {
            if (_storePlayer != null)
            {
                //if(_storePlayer.GetComponent<BeadGutiAISystem>()._isAccessForBead == true){
                _storePlayer.transform.position = transform.position;
               // Vector3 _smoothPos = _storePlayer.transform.position;
              //  _storePlayer.transform.position = Vector3.Slerp(_smoothPos, transform.position, 2f);
                //  _storePlayer.GetComponent<BeadGutiAISystem>()._isAccessForBead = false;
                //  }
                if (_destoryingObject != null)
                {

                    if (_isPhotonMultiplayer == false)
                    {
                        GameManager._gameManager._playerIndivisualTimeIndex = 40;
                        Instantiate(GameManager._gameManager._destoryEffect, _destoryingObject.transform.position, Quaternion.identity);
                        KillUpdate(_storePlayer.GetComponent<BeadGutiAISystem>()._userToken);
                        Destroy(_destoryingObject);
                        
                        if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
                        {
                            _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = false;
                        }

                        //VibrateControll
                        VibrateControll();

                    }
                    else
                    {
                        if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
                        {                           
                            _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = false;
                        }

                        PlayerNetworking_SGDSOFT _network = _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>();
                        _network.NetworkPlayerDestory(_destoryingObject.GetComponent<PhotonView>().viewID, _storePlayer.GetComponent<BeadGutiAISystem>()._userToken);
                        

                        for (int i = 0; i < BeadBoardManager._BeadBoardManager._childs.Length; i++)
                        {
                            if (BeadBoardManager._BeadBoardManager._childs[i].gameObject.name != this.gameObject.name)
                            {
                                BeadBoardManager._BeadBoardManager._childs[i].GetComponent<BeadPosition>()._storePlayer = null;
                            }
                        }

                    }

                }
                else
                {
                    if (_isPhotonMultiplayer == false)
                    {

                        if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
                        {
                            _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = false;
                        }

                        GameManager._gameManager._playerIndivisualTimeIndex = 40;
                        GameManager._gameManager.SwithPlayerManagerButton();
                    }
                    else
                    {
                        if (_storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
                        {
                            _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<AIController>().enabled = false;
                        }

                        PlayerNetworking_SGDSOFT _network = _storePlayer.GetComponent<BeadGutiAISystem>()._playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>();
                        _network.TurnControll();
                    }
                }



            }
            else
            {
                return;
            }

            for (int i = 0; i < BeadBoardManager._BeadBoardManager._childs.Length; i++)
            {
                if (BeadBoardManager._BeadBoardManager._childs[i].gameObject.name != this.gameObject.name)
                {
                    BeadBoardManager._BeadBoardManager._childs[i].GetComponent<BeadPosition>()._storePlayer = null;
                }
            }
        }
        
    }


    void KillUpdate(string _token)
    {
        for (int i = 0; i < GameManager._gameManager._playerBoard.Count; i++)
        {
            if (GameManager._gameManager._playerBoard[i]._userToken == _token)
            {
                GameManager._gameManager._playerBoard[i]._totalKill++;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.tag == "Bead"){
            other.gameObject.GetComponent<BeadGutiAISystem>()._up = _up;
            other.gameObject.GetComponent<BeadGutiAISystem>().down = down;
            other.gameObject.GetComponent<BeadGutiAISystem>().left = left;
            other.gameObject.GetComponent<BeadGutiAISystem>().right = right;
            other.gameObject.GetComponent<BeadGutiAISystem>().up_left = up_left;
            other.gameObject.GetComponent<BeadGutiAISystem>().up_right = up_right;
            other.gameObject.GetComponent<BeadGutiAISystem>().down_left = down_left;
            other.gameObject.GetComponent<BeadGutiAISystem>().down_right = down_right;
        }
    }

  


    //Vibrate
    void VibrateControll(){
        if(PlayerPrefs.GetInt("Vibrate") == 1){
            Handheld.Vibrate();
        }
    }

    

}
