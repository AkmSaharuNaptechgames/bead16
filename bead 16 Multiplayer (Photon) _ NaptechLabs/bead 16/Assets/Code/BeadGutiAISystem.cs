﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SGDSOFT;

[System.Serializable]
public enum Player { 
    Green,Blue
}

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class BeadGutiAISystem : MonoBehaviour
{
    public bool _isAccessForBead;
    public string _userToken;
    public Player _userTurnToken;
    public string _userName;

    public GameObject _heighLightSprite;

    [Header("Don't Touch Here")]
    public Transform _UP_Left;
    public Transform _UP_Right;
    public Transform _DOWN_Left;
    public Transform _DOWN_Right;
    public float _angle;
    public PlayerManager _playerManager;

    public bool _up,down,left,right,up_left,up_right,down_left,down_right;
    public bool _isBlocked;

    //private call
    private GameObject _selectPlayer;

    //AI Call
     public bool _readyForAIMove;

    private void Start()
    {

        //Init
        _playerManager = GetComponentInParent<PlayerManager>();

        if (!_playerManager.gameObject.GetComponent<PlayerNetworking_SGDSOFT>())
        {
            switch (_userTurnToken)
            {
                case Player.Green:
                    _playerManager.SetData(_userName, true, _userToken, false, "");
                    break;
                case Player.Blue:
                    _playerManager.SetData(_userName, false, "", true, _userToken);
                    break;
            }
        }
    }

    void Update()
    {

        Vector3 _pos = transform.position;
        _pos.z = 0;
        transform.position = _pos;

        switch (_userTurnToken)
        {
            case Player.Green:
                if (GameManager._gameManager._greenPlayer == true)
                {
                    AI();
                }
                break;
            case Player.Blue:
                if (GameManager._gameManager._bluePlayer == true)
                {
                    AI();
                }
                break;
        }

        //BeadGuti Click
        /*RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if(hit.collider != null && !_playerManager.gameObject.GetComponent<AIController>())
        {
            try{
                if(hit.collider.name == this.gameObject.name){
                    print(hit.collider.name);
                    _playerManager.Access(this.gameObject.name);
                }
            }catch{
                print("Not Transform Target");
            }
            
        }*/
        //BeadGuti Click
        if (Input.GetMouseButtonDown(0) && !_playerManager.gameObject.GetComponent<AIController>().enabled)
        {
            if (GameManager._gameManager._isPhotonMultiplayer == false)
            {
                if (GameManager._gameManager._isRobotTurn == false)
                {
                    Debug.Log("Clicked");
                    Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                    RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
                    // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
                    if (hitInfo)
                    {
                        Debug.Log(hitInfo.transform.gameObject.name);
                        if (!_playerManager.gameObject.GetComponent<AIController>().enabled)
                        {
                            try
                            {
                                if (hitInfo.transform.gameObject.name == this.gameObject.name)
                                {
                                    _playerManager.Access(this.gameObject.name);
                                }
                            }
                            catch
                            {
                                print("Not Transform Target");
                            }

                        }

                    }
                }
            }
            else
            {
                Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
         
                if (hitInfo)
                {
                    Debug.Log(hitInfo.transform.gameObject.name);
                    try
                    {
                        if (hitInfo.transform.gameObject.name == this.gameObject.name)
                        {
                            _playerManager.Access(this.gameObject.name);
                        }
                    }
                    catch
                    {
                        print("Not Transform Target");
                    }

                }
            }
        }

       


    }

    
    

    private void AI()
    {
 

        if(_isAccessForBead == true || (_playerManager.gameObject.GetComponent<AIController>().enabled && _playerManager.gameObject.GetComponent<AIController>()._isRayHit == true)){
            //AI of Left Side
            print("A");
            if(_up == true){
                UpAI(transform.position + new Vector3(0,15f,0));
            }
            if(down == true){
                DownAI(transform.position + new Vector3(0,-15f,0));
            }
            if(left == true){
                LeftAI(transform.position + new Vector3(-15f,0,0));
            }
            if(right == true){
                RightAI(transform.position + new Vector3(15f,0,0));
            }
            if(up_left == true){
                Up_45Pla_AI(transform.position + new Vector3(-15f,15f,0));
            }
            if(up_right == true){
                Up_45Min_AI(transform.position + new Vector3(15f,15f,0));
            }
            if(down_left == true){
                Down_45Pla_AI(transform.position + new Vector3(15f,-15f,0));
            }
            if(down_right == true){
                Down_45Min_AI(transform.position + new Vector3(-15f,-15f,0));
            }
                     
            

        }
        
        
    }
     

    void LeftAI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + Vector3.left * 0.2f;
       
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + Vector3.left * 0.2f;
                         Debug.DrawLine(_pos_left_Again,_direction,Color.green);
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }
   void RightAI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + Vector3.right * 0.2f;
        
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + Vector3.right * 0.2f;
                         Debug.DrawLine(_pos_left_Again,_direction,Color.green);
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }

    void UpAI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + Vector3.up * 0.2f;
        Debug.DrawLine(_posLeft,_direction,Color.green);
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction * 2f))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + Vector3.up * 0.2f;
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction * 2f))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }
   void DownAI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + Vector3.down * 0.2f;
        Debug.DrawLine(_posLeft,_direction,Color.green);
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction * 2f))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + Vector3.down * 0.2f;
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction * 2f))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }

    //Cross
    void Up_45Pla_AI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + new Vector3(-0.18f,0.18f,0);
       
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction))
        {
             Debug.DrawLine(_posLeft,_direction,Color.green);
             print(_hit_A.collider.name);
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + new Vector3(-0.18f,0.18f,0);
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }


    void Up_45Min_AI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + new Vector3(0.18f,0.18f,0);
        Debug.DrawLine(_posLeft,_direction,Color.green);
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + new Vector3(0.18f,0.18f,0);
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction))
                        {
                            if (_hit_A_Again.collider != null )
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }

   void Down_45Pla_AI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + new Vector3(0.18f,-0.18f,0);
        Debug.DrawLine(_posLeft,_direction,Color.green);
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + new Vector3(0.18f,-0.18f,0);
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }


    void Down_45Min_AI(Vector3 _direction)
    {
        RaycastHit2D _hit_A;
        Vector3 _posLeft = transform.position + new Vector3(-0.18f,-0.18f,0);
        Debug.DrawLine(_posLeft,_direction,Color.green);
        if (_hit_A = Physics2D.Linecast(_posLeft, _direction))
        {
            if (_hit_A.collider != null)
            {
                if (_hit_A.collider.tag == "Bead")
                {
                    if (_hit_A.collider.gameObject.GetComponent<BeadGutiAISystem>()._userToken != _userToken)
                    {
                        print(_hit_A.collider.gameObject.name);
                        RaycastHit2D _hit_A_Again;
                        Vector3 _pos_left_Again = _hit_A.collider.gameObject.transform.position + new Vector3(-0.18f,-0.18f,0);
                        if (_hit_A_Again = Physics2D.Linecast(_pos_left_Again, _direction))
                        {
                            if (_hit_A_Again.collider != null)
                            {
                                print(_hit_A_Again.collider.gameObject.name);
                                try
                                {
                                    if (_hit_A_Again.collider.tag != "Bead" && _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>()._isNextHit == true)
                                    {
                                        _hit_A_Again.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, _hit_A.collider.gameObject);
                                    }
                                    else
                                    {
                                        _isBlocked = true;
                                    }
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    try
                    {
                        _hit_A.collider.gameObject.GetComponent<BeadPosition>().PlayerTransformRaycastMouseHit(this.gameObject, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }




    

}
