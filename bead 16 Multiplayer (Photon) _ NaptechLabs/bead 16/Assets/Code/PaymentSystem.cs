﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Beating
{
    public int _minimumNeedForBeating;
}




public class PaymentSystem : MonoBehaviour
{
    public static PaymentSystem _paymentSystem { get; set; }

    public Beating _beating;

    private void Awake()
    {
        _paymentSystem = this;
    }


   public void ShowPaymentPanel()
    {
        print("The Payment panel will be show.");
    }

}
