﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [Header("Don't Touch")]
    public Transform[] _childs;
    public bool _blue;
    public bool _green;
    public int _totalBlocked;
   
    public Sprite _storeSprite;

    //private call
    private bool _OneLoop_A;
    private bool _OneLoop_B;
     private bool _OneLoop_C;
    // Start is called before the first frame update
    void Start()
    {
        _OneLoop_B = false;
        _OneLoop_C = false;
    }

    public void SetData(string _userName,bool _green,string _green_userToken,bool _blue,string _blue_userToken){
        if(_OneLoop_A == false){
            if(_green == true && _blue == false){
                
                //SetupName For Robot
                if(GetComponent<AIController>().enabled){
                    string[] _RobotName = new string[4];
                    _RobotName[0] = "Akm";
                    _RobotName[1] = "Saharu";
                    _RobotName[2] = "Zzaman";
                    _RobotName[3] = "Rakhit";
                    _userName = "" + _RobotName[Random.Range(0,3)];
                    _storeSprite = GameManager._gameManager._uiInterfacing._avatars[Random.Range(0,3)];
                }else{
                    _userName = "" + PlayerPrefs.GetString("UserName");
                    _storeSprite = GameManager._gameManager._uiInterfacing._avatars[PlayerPrefs.GetInt("AvatarSerialNumber")];
                }

                GameManager._gameManager._playerBoard.Add(new PlayerBoard
                {
                    _userToken = _green_userToken,
                    _gutiID = "Green",
                    _userName = _userName,
                    _totalKill = 0,
                    _avatar = _storeSprite
                });



                _green = true;
                _OneLoop_A = true;
            }
            if(_green == false && _blue == true){

                //SetupName For Robot
                if(GetComponent<AIController>().enabled){
                    string[] _RobotName = new string[4];
                    _RobotName[0] = "Akm";
                    _RobotName[1] = "Saharu";
                    _RobotName[2] = "Zzaman";
                    _RobotName[3] = "Rakhit";
                    _userName = "" + _RobotName[Random.Range(0,3)];
                    _storeSprite = GameManager._gameManager._uiInterfacing._avatars[Random.Range(0,3)];
                }else{
                    _userName = "" + PlayerPrefs.GetString("UserName");
                    _storeSprite = GameManager._gameManager._uiInterfacing._avatars[PlayerPrefs.GetInt("AvatarSerialNumber")];
                }

                GameManager._gameManager._playerBoard.Add(new PlayerBoard
                {
                    _userToken = _blue_userToken,
                    _gutiID = "Blue",
                    _userName = _userName,
                    _totalKill = 0,
                    _avatar = _storeSprite
                });
                _blue = true;
                _OneLoop_A = true;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
      //  _totalBlocked = 0;
        /*for (int i = 0; i < _childs.Length; i++)
        {
            if(_childs[i].gameObject.GetComponent<BeadGutiAISystem>()._isBlocked == true)
            {
                _totalBlocked++;
            }
        }*/

        if(_totalBlocked == _childs.Length)
        {
          //  GameManager._gameManager._isWinnerResult = true;
        }

        if (GameManager._gameManager._isPhotonMultiplayer == false)
        {

            if (GameManager._gameManager._bluePlayer == true)
            {
                if (_blue == true)
                {
                    if (GetComponent<AIController>().enabled)
                    {
                        GameManager._gameManager._isRobotTurn = true;
                    }
                    else
                    {
                        GameManager._gameManager._isRobotTurn = false;
                    }
                }
            }
            if (GameManager._gameManager._greenPlayer == true)
            {
                if (_green == true)
                {
                    if (GetComponent<AIController>().enabled)
                    {
                        GameManager._gameManager._isRobotTurn = true;
                    }
                    else
                    {
                        GameManager._gameManager._isRobotTurn = false;
                    }
                }
            }
        }
       

        realTimeUpdate();

        if(GameManager._gameManager._bluePlayer == true){
            print("OK");
            if(_green == true){
                print("OK_A");
                for(int i = 0; i < _childs.Length; i++){
                    _childs[i].GetComponent<BeadGutiAISystem>()._heighLightSprite.SetActive(false);
                }

            }
        }


        if(GameManager._gameManager._greenPlayer == true){
            if(_blue == true){
                for(int i = 0; i < _childs.Length; i++){
                    _childs[i].GetComponent<BeadGutiAISystem>()._heighLightSprite.SetActive(false);
                }

            }
        }

        if(!this.gameObject.GetComponent<PhotonView>()){
            if(_childs.Length <= 0){
                if(_OneLoop_B == false){
                    GameManager._gameManager._isWinnerResult = true;
                    _OneLoop_B = true;
                }
            }
        }else{
            
            if(_childs.Length <= 0){
                if(_OneLoop_B == false){
                    this.gameObject.GetComponent<PlayerNetworking_SGDSOFT>().GameOverLobby();
                    this.gameObject.GetComponent<PlayerNetworking_SGDSOFT>().enabled = false;
                    GameManager._gameManager._isWinnerResult = true;
                    _OneLoop_B = true;
                }
            }
        }

        if(GameManager._gameManager._isWinnerResult == true){
            if(_OneLoop_C == false){
                GameManager._gameManager.enabled = false;
                for(int i = 0; i < _childs.Length; i++){
                    _childs[i].gameObject.SetActive(false);   
                }
                GameManager._gameManager._gamePlayPanel.SetActive(false);
                GameManager._gameManager._WinnerPanel.SetActive(true);
                _OneLoop_C = true;
            }
        }

        
    }

    void realTimeUpdate(){
        _childs = new Transform[transform.childCount];
        int _index = 0;
        foreach(Transform t in transform){
            _childs[_index++] = t;
        }
    }
    
    public void Access(string _IdentyName){
        for(int i = 0; i < _childs.Length; i++){
            if(_childs[i].GetComponent<BeadGutiAISystem>().gameObject.name == _IdentyName){
                _childs[i].GetComponent<BeadGutiAISystem>()._isAccessForBead = true;
                _childs[i].GetComponent<BeadGutiAISystem>()._heighLightSprite.SetActive(true);
            }else{
                _childs[i].GetComponent<BeadGutiAISystem>()._isAccessForBead = false;
                _childs[i].GetComponent<BeadGutiAISystem>()._heighLightSprite.SetActive(false);
            }
        }
        
    }


}
