﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

[System.Serializable]
public class OfflineController{
    [Header("UI")]
    public GameObject _offlinePanel;
    public GameObject _userNamePanel;
    public GameObject _selectCharacterPanel;
    public GameObject _dashboardPanel;
    public TMP_InputField _userNameInputPanel;
    [Header("Don't Touch Here")]
    public int _userAvatarSerialNumber;
    public string _userName;
    
}

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController _MainMenuController {get;set;}
    public GameObject _joinPanel;
    [Header("Avatars")]
    public List<GameObject> _selectAvatars;
    public Sprite[] _avaters;
    [Header("Offline Panel")]
    public OfflineController _OfflineController;

    [Header("Global Play Button")]
    public Button _GlobalPhotonOnlineButton;
    [Header("User")]
    public Image _pic;

    private void Awake() {
        _MainMenuController = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("CreateAccount") == 0){
            _joinPanel.SetActive(true);
            _OfflineController._offlinePanel.SetActive(false);
            _OfflineController._userNamePanel.SetActive(true);
            _OfflineController._selectCharacterPanel.SetActive(false);
            _OfflineController._dashboardPanel.SetActive(false);
            SelectAvatar(0);
        }else{
            _joinPanel.SetActive(false);
            _OfflineController._offlinePanel.SetActive(false);
            _OfflineController._dashboardPanel.SetActive(true);
            SelectAvatar(0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // /OfflineUIUpdate
        OfflineUIUpdate();

        //check Online And Create Account
        if(Application.internetReachability == NetworkReachability.NotReachable){
            _GlobalPhotonOnlineButton.interactable = false;
        }else{
            _GlobalPhotonOnlineButton.interactable = true;
        }

        _pic.sprite = _avaters[PlayerPrefs.GetInt("AvatarSerialNumber")];

    }

    void OfflineUIUpdate(){
         _OfflineController._userName = _OfflineController._userNameInputPanel.text;
    }

    public void SelectAvatar(int index){
        for(int i = 0; i < _selectAvatars.Count; i++){
            _selectAvatars[i].SetActive(false);
        }
        _selectAvatars[index].SetActive(true);
        _OfflineController._userAvatarSerialNumber = index;
        DatabaseController._DatabaseController._playerAvatarID = index;
    }
    
    public void OffineCreateAccountUserNameButton(){
        if(_OfflineController._userNameInputPanel.text.Length > 0){
            _OfflineController._userNamePanel.SetActive(false);
            _OfflineController._selectCharacterPanel.SetActive(true);
        }
    }

    public void OffineCreateAccountButton(){
        PlayerPrefs.SetString("UserName",_OfflineController._userName);
        PlayerPrefs.SetInt("AvatarSerialNumber",_OfflineController._userAvatarSerialNumber);
        PlayerPrefs.SetInt("CreateAccount",1);
        _OfflineController._offlinePanel.SetActive(false);
        _OfflineController._dashboardPanel.SetActive(true);
    }

   public void StartGamePlay(int scenesIndex){
       SceneManager.LoadScene(scenesIndex);
   }

   public void LogOut(){
        PlayerPrefs.DeleteAll();
        _joinPanel.SetActive(true);
        _OfflineController._offlinePanel.SetActive(false);
        _OfflineController._userNamePanel.SetActive(true);
        _OfflineController._selectCharacterPanel.SetActive(false);
        _OfflineController._dashboardPanel.SetActive(false);
        SelectAvatar(0);
   }
    public void ExitThisGame() {

                #if UNITY_EDITOR
                        UnityEditor.EditorApplication.isPlaying = false;
                #else
                    Application.Quit();
        
                #endif

        }

    public void PlayMultiplayGame(int scenesIndex)
    {
        if(PlayerPrefs.GetString("UserID").Length > 0)
        {
            if(int.Parse(PlayerPrefs.GetString("UserScore")) > PaymentSystem._paymentSystem._beating._minimumNeedForBeating)
            {
                SceneManager.LoadScene(scenesIndex);
            }
            else
            {
                print("Show Payment Panel");
            }
        }
    }

}
