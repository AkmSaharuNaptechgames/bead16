﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SGDSOFT;
public class AIController : MonoBehaviour
{
    public static AIController _AIController{get;set;}

    [Header("Don't Touch Here")]
    public bool _isRayHit;
    public bool _isMove;
    public List<GameObject> _RayHitList;
    public bool _isOneLoop_A;
    public bool _isOneLoop_B;

    //private call
    private PlayerManager _playerManager;

    private void Awake() {
        _AIController = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        _playerManager = GetComponent<PlayerManager>();
        _isOneLoop_A = false;
        _isOneLoop_B = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(_playerManager._blue == true){

            if(GameManager._gameManager._bluePlayer == true && _isRayHit == false){
                Invoke("TimeToStartRayHIT",1f);
            }
          
        }
       /* if (GameManager._gameManager._bluePlayer == true && _isMove == true)
        {
            if (GameManager._gameManager._playerIndivisualTimeIndex < 20f)
            {
                _isMove = false;
                TimeToStartRayHIT();
            }
        }*/

        if (_playerManager._green == true){

            if(GameManager._gameManager._greenPlayer == true && _isRayHit == false){
                Invoke("TimeToStartRayHIT",1f);
            }          

        }

        if (GetComponent<PlayerNetworking_SGDSOFT>().enabled = true)
        {
            if (_RayHitList.Count > 1500)
            {
                _RayHitList.Clear();
                for (int i = 0; i < BeadBoardManager._BeadBoardManager._childs.Length; i++)
                {
                    if (BeadBoardManager._BeadBoardManager._childs[i].gameObject.name != this.gameObject.name)
                    {
                        BeadBoardManager._BeadBoardManager._childs[i].GetComponent<BeadPosition>()._storePlayer = null;
                    }
                }
                _isRayHit = false;
                _isMove = true;
            }
        }

    }

    void TimeToStartRayHIT(){
        _isRayHit = true;
        _isMove = false;
        //TimeToMarkGuti();
        Invoke("TimeToMarkGuti",1f);
    }

    void TimeToMarkGuti(){
        try
        {
            if (_RayHitList.Count > 0 && _isOneLoop_A == false)
            {
                int _chooseNumber = Random.Range(0, _RayHitList.Count - 1);
                if (_RayHitList[_chooseNumber] == null)
                {
                    _chooseNumber = Random.Range(0, _RayHitList.Count - 1);
                }
                else
                {
                    _RayHitList[_chooseNumber].GetComponent<BeadGutiAISystem>()._readyForAIMove = true;
                    _isOneLoop_A = true;
                    _isOneLoop_B = false;
                }

            }
        }
        catch
        {
            Destroy(GetComponent<AIController>());
        }
    }

    

}
