﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGDSOFT{
    public class BeadBoardManager : MonoBehaviour
    {
        public static BeadBoardManager _BeadBoardManager {get;set;}
        public float _space;
        public float _GizmosSphere_Size;
       
        public Color _lineColor;

         public Transform[] _childs;
        //private call
       
        private Vector3[] _startPos;
        private Vector3[] _startPos_Left;
        private Vector3[] _startPos_Right;

        // Start is called before the first frame update
        void Awake()
        {
            _BeadBoardManager = this;
        }

        private void Start() {
            _childs = new Transform[transform.childCount];
            int _index = 0;
            foreach(Transform t in transform){
                _childs[_index++] = t;
            }
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void OnDrawGizmos() {    
            _startPos = new Vector3[9];
            _startPos_Left = new Vector3[2];
            _startPos_Right = new Vector3[2];
            Gizmos.color = Color.white;
            if(_space == 0){
                _space = 2;
            }
            if(_GizmosSphere_Size == 0){
                _GizmosSphere_Size = 0.2f;
            }
            
            Vector3 _pre = new Vector3(0,-_space,0);
            if(_startPos.Length > 0){
                for(int i = 0; i < _startPos.Length; i++){
                    if(i != 0){
                        //Display
                        Gizmos.DrawLine(_pre,_pre + Vector3.up * _space);
                    }
                    _startPos[i] = _pre + Vector3.up * _space;
                    _pre = _startPos[i];
                    Gizmos.DrawSphere(_startPos[i],_GizmosSphere_Size);
                    

                    int _mid = (_startPos.Length - 0) / 2;

                    //Left
                    if(_startPos_Left.Length > 0){
                        Vector3 _pre_Mid_Left = _startPos[i];
                        if( i == 0 || i == _startPos.Length - 1){
                            for(int j = 0; j < _startPos_Left.Length - 1; j++){
                                //Display
                                Gizmos.DrawLine(_pre_Mid_Left,_pre_Mid_Left + Vector3.left * _space * 1.5f);
                                _startPos_Left[j] = _pre_Mid_Left + Vector3.left * _space * 1.5f;
                                _pre_Mid_Left = _startPos_Left[j];
                                Gizmos.DrawSphere(_startPos_Left[j],_GizmosSphere_Size);

                            }
                        }else if(i == 1 || i == _startPos.Length - 2){
                            for(int j = 0; j < _startPos_Left.Length - 1; j++){
                                //Display
                                Gizmos.DrawLine(_pre_Mid_Left,_pre_Mid_Left + Vector3.left * _space);
                                _startPos_Left[j] = _pre_Mid_Left + Vector3.left * _space;
                                _pre_Mid_Left = _startPos_Left[j];
                                Gizmos.DrawSphere(_startPos_Left[j],_GizmosSphere_Size);

                            }
                        }else{
                            for(int j = 0; j < _startPos_Left.Length; j++){
                                //Display
                                Gizmos.DrawLine(_pre_Mid_Left,_pre_Mid_Left + Vector3.left * _space);
                                _startPos_Left[j] = _pre_Mid_Left + Vector3.left * _space;
                                _pre_Mid_Left = _startPos_Left[j];
                                Gizmos.DrawSphere(_startPos_Left[j],_GizmosSphere_Size);

                            }
                        }
                    }

                    //Right
                    if(_startPos_Right.Length > 0){
                        Vector3 _pre_Mid_Right = _startPos[i];
                        if(i == 0 || i == _startPos.Length - 1){            
                            for(int j = 0; j < _startPos_Right.Length - 1; j++){
                                //Display
                                Gizmos.DrawLine(_pre_Mid_Right,_pre_Mid_Right + -Vector3.left * _space * 1.5f);
                                _startPos_Right[j] = _pre_Mid_Right + -Vector3.left * _space * 1.5f;
                                _pre_Mid_Right = _startPos_Right[j];
                                Gizmos.DrawSphere(_startPos_Right[j],_GizmosSphere_Size);

                            }
                        }else if(i == 1 || i == _startPos.Length - 2){
                            for(int j = 0; j < _startPos_Right.Length - 1; j++){
                                //Display
                                Gizmos.DrawLine(_pre_Mid_Right,_pre_Mid_Right + -Vector3.left * _space);
                                _startPos_Right[j] = _pre_Mid_Right + -Vector3.left * _space;
                                _pre_Mid_Right = _startPos_Right[j];
                                Gizmos.DrawSphere(_startPos_Right[j],_GizmosSphere_Size);

                            }
                        }else{
                            for(int j = 0; j < _startPos_Right.Length; j++){
                                //Display
                                Gizmos.DrawLine(_pre_Mid_Right,_pre_Mid_Right + -Vector3.left * _space);
                                _startPos_Right[j] = _pre_Mid_Right + -Vector3.left * _space;
                                _pre_Mid_Right = _startPos_Right[j];
                                Gizmos.DrawSphere(_startPos_Right[j],_GizmosSphere_Size);

                            }
                        }
                    }

                
                }
            }

        }


    }

}
