﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System;

[System.Serializable]
public enum PlayersSelection { 
    Green,Blue
}

[RequireComponent(typeof(PhotonView))]
public class PlayerNetworking_SGDSOFT : MonoBehaviour {


	[SerializeField]private MonoBehaviour[] scriptsToIgnore;
	public int _playerPositionIndex;
	public PlayerManager _PlayerManager;
	public PlayersSelection _userTurnToken;

	[Header("Find Friend Timer")]
	public bool _isFindFriends;
	public int _findFriendTimerIndex;
	private float _addFindFriendTimerIndex;

	[Header("Don't Touch Here")]
	public string _name;
	public int _AvatarSerialNumber;
	public int _time;
	public int _maximumJoinningPlayer;
	public bool _isOwner;
	public bool _OneLoop_A;
	public bool _OneLoop_B;
	public bool _OneLoop_C;
	public bool _OneLoop_D;
    public bool _OneLoop_E;
    public bool _OneLoop_F;

    private PhotonView photonView;

	// Use this for initialization
	void Start () {
		
		_OneLoop_A = false;
		_OneLoop_C = false;
		_OneLoop_D = false;
		_isFindFriends = false;
        _OneLoop_E = false;
        _OneLoop_F = false;
        photonView = GetComponent<PhotonView> ();
		Initialize ();
		
	}
	void Initialize(){
		if (photonView.isMine) {
            
			for(int i = 0; i < PhotonManager_SGDSOFT._photonManager_SGDSOFT._EmojsButton.Length; i++){
				Emoj _emoj = PhotonManager_SGDSOFT._photonManager_SGDSOFT._EmojsButton[i].gameObject.GetComponent<Emoj>();
				PhotonManager_SGDSOFT._photonManager_SGDSOFT._EmojsButton[i].onClick.AddListener(delegate{ServerEmojPushINT(_emoj);});
			}
		} else {
			/*for(int i = 0; i < _PlayerManager._childs.Length; i++){
					//_PlayerManager._childs[i].gameObject.GetComponent<BeadGutiAISystem>().enabled = false;
				}*/
			foreach (MonoBehaviour item in scriptsToIgnore) {
				item.enabled=false;
			}
		}
	}

	
	private void Update(){

        if (!photonView.isMine)
        {
            this.GetComponent<PlayerNetworking_SGDSOFT>().enabled = false;
        }

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (photonView.isMine)
            {
                
             
                if (_isReadyALL() == true)
                {
                    print("READY");
                    //PhotonManager_SGDSOFT._photonManager_SGDSOFT._AllPlayerReady = true;

                    if (PhotonManager_SGDSOFT._photonManager_SGDSOFT._AllPlayerReady == true)
                    {
                        //PhotonManager_SGDSOFT.ShareShareCounting(photonView);
                        if (_OneLoop_D == false)
                        {
                            //Invoke("TimeToCallForCountDown",0.5f);
                           // TurnControll();
                            _OneLoop_D = true;
                        }

                    }
                }
                else
                {
                    print("NOT READY");
                }

                if (_isOwner == false && GameManager._gameManager._playerBoard.Count > 0)
                {
                    if (PhotonManager_SGDSOFT._photonManager_SGDSOFT._AllPlayerReady == true)
                    {
                        //Set user INFO
                        if (_OneLoop_A == false)
                        {
                            //PhotonManager_SGDSOFT.ShareUSER_INFO(photonView,_name,_rank,_time);
                            PhotonManager_SGDSOFT._photonManager_SGDSOFT._loadingPanel.SetActive(true);
                            SendUserInfo();
                            _OneLoop_A = true;
                        }
                    }
                    else
                    {
                        PhotonManager_SGDSOFT._photonManager_SGDSOFT._loadingPanel.SetActive(true);
                    }
                }

                if (_isOwner == true)
                {
                    if (PhotonManager_SGDSOFT._photonManager_SGDSOFT._AllPlayerReady == true)
                    {
                        if (GameManager._gameManager._playerBoard.Count >= 2)
                        {
                            if (_OneLoop_C == false)
                            {
                                //SendUserInfo();
                                photonView.RPC("SendStartGameWave", PhotonTargets.AllBuffered);
                                _OneLoop_C = true;
                            }
                        }
                    }

                }

                if (PhotonManager_SGDSOFT._photonManager_SGDSOFT._StartRace == true)
                {
                    if (_OneLoop_B == false)
                    {
                        for (int i = 0; i < _PlayerManager._childs.Length; i++)
                        {
                            _PlayerManager._childs[i].gameObject.SetActive(true);
                        }
                        GameManager._gameManager._isTimer = true;
                        PhotonManager_SGDSOFT._photonManager_SGDSOFT._loadingPanel.SetActive(false);
                        _OneLoop_B = true;
                    }
                }
                else
                {
                    for (int i = 0; i < _PlayerManager._childs.Length; i++)
                    {
                        _PlayerManager._childs[i].gameObject.SetActive(false);
                    }
                }



            }




            if (PhotonManager_SGDSOFT._photonManager_SGDSOFT._StartRace == true)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    if (PhotonNetwork.playerList.Length >= 2)
                    {
                        if (GameManager._gameManager._playerBoard.Count >= 2)
                        {
                            if(GameManager._gameManager._greenPlayer == true)
                            {
                                GameManager._gameManager._green_Turn.enabled = true;
                                GameManager._gameManager._blue_Turn.enabled = false;
                            }
                            else
                            {
                                GameManager._gameManager._blue_Turn.enabled = true;
                                GameManager._gameManager._green_Turn.enabled = false;
                            }

                            if (GameManager._gameManager._addNextIndivisualTime < Time.time)
                            {
                                GameManager._gameManager._playerIndivisualTimeIndex--;
                                if(GameManager._gameManager._playerIndivisualTimeIndex <= 0)
                                {
                                    GameManager._gameManager._playerIndivisualTimeIndex = 0;
                                }
                                if (GameManager._gameManager._playerIndivisualTimeIndex <= 0)
                                {                              
                                    _PlayerManager.gameObject.GetComponent<AIController>().enabled = true;
                                }
                                else
                                {
                                    _PlayerManager.gameObject.GetComponent<AIController>().enabled = false;
                                }
                                

                                TimeToCallForCountDown(GameManager._gameManager._playerIndivisualTimeIndex);
                                
                                GameManager._gameManager._addNextIndivisualTime = Time.time + 0.5f;
                            }
                        }
                        else
                        {
                            GameManager._gameManager._isWinnerResult = true;
                        }
                    }
                    else
                    {
                        GameManager._gameManager._isWinnerResult = true;
                    }
                }
                else
                {
                    if(PhotonNetwork.playerList.Length >= 2)
                    {
                        if(GameManager._gameManager._playerBoard.Count >= 2)
                        {
                            if (GameManager._gameManager._bluePlayer == true)
                            {
                                GameManager._gameManager._blue_Turn.enabled = false;
                                GameManager._gameManager._green_Turn.enabled = true;
                            }
                            else
                            {
                                GameManager._gameManager._green_Turn.enabled = false;
                                GameManager._gameManager._blue_Turn.enabled = true;
                            }
                            print("ROBOT A");
                            if (GameManager._gameManager._clientTime_ForMultiplayer <= 0)
                            {
                                print("ROBOT B");
                                _PlayerManager.gameObject.GetComponent<AIController>().enabled = true;
                            }
                            else
                            {
                                _PlayerManager.gameObject.GetComponent<AIController>().enabled = false;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (PhotonManager_SGDSOFT._photonManager_SGDSOFT._StartRace == true)
            {
                PhotonManager_SGDSOFT._photonManager_SGDSOFT.ExitLobby(1);
            }
            else
            {
                SceneManager.LoadScene(2);
            }

        }


	}

	public bool _isReadyALL(){
		bool _result = false;
		if(photonView.isMine){
			if(_isOwner == true){
				if(_result == false){
					
					if(PhotonNetwork.playerList.Length >= _maximumJoinningPlayer){
						//Set user INFO
						if(_OneLoop_A == false){
                            //PhotonManager_SGDSOFT.ShareUSER_INFO(photonView,_name,_rank,_time);
                            SendUserInfo();
                            
                            photonView.RPC("SendAllPlayerReadyCallBack",PhotonTargets.AllBuffered);
							
							if(PhotonNetwork.isMasterClient){
								PhotonNetwork.room.visible = false;
								PhotonNetwork.room.open = false;
                                
								_isFindFriends = true;
							}

							_OneLoop_A = true;
						}
						_result = true;
					}else{
						if(_isFindFriends == false){
							PhotonManager_SGDSOFT._photonManager_SGDSOFT._loadingPanel.SetActive(true);
							if(_addFindFriendTimerIndex < Time.time){
								_findFriendTimerIndex++;
								if(_findFriendTimerIndex >= 30f){
                                    
									PhotonNetwork.LeaveLobby();
									PhotonNetwork.Disconnect();
									if(PhotonNetwork.connected == false){
										SceneManager.LoadScene(2);
									}
								}
								_addFindFriendTimerIndex = Time.time + 1f;
							}
						}
					}
				}
			}
		}
		return _result;
	}

	void SendUserInfo(){

        GameManager._gameManager._userOwnMultiplayerNamePosition = _name;

        switch (_userTurnToken)
       	{
           case PlayersSelection.Green:
		   		photonView.RPC("SendInformation",PhotonTargets.AllBuffered,_name,true,"Green",false,"","Green",_AvatarSerialNumber);
               //_playerManager.SetData(_userName,true,_userToken,false,"");
			   //photonView.RPC("ShareUserInfo",PhotonTargets.AllBuffered,_name,true,"Green",false,"","Green");
               break;
           case PlayersSelection.Blue:
		   		photonView.RPC("SendInformation_A", PhotonTargets.AllBuffered,_name,false,"",true,"Blue","Blue",_AvatarSerialNumber);
		   		//photonView.RPC("ShareUserInfo",PhotonTargets.AllBuffered,_name,false,"",true,"Blue","Blue");
               break;
       	}
	}

	[PunRPC]
	void SendInformation(string _userName,bool _green,string _green_userToken,bool _blue,string _blue_userToken,string _GutiID,int _avatarSerialNumber){
		GameManager._gameManager._playerBoard.Add(new PlayerBoard
                {
                    _userToken = _green_userToken,
                    _gutiID = _GutiID,
                    _userName = _userName,
                    _totalKill = 0,
                    _avatar = GameManager._gameManager._uiInterfacing._avatars[_avatarSerialNumber]
                });
	}
    [PunRPC]
    void SendInformation_A(string _userName, bool _green, string _green_userToken, bool _blue, string _blue_userToken, string _GutiID, int _avatarSerialNumber)
    {
        GameManager._gameManager._playerBoard.Add(new PlayerBoard
        {
            _userToken = _blue_userToken,
            _gutiID = _GutiID,
            _userName = _userName,
            _totalKill = 0,
            _avatar = GameManager._gameManager._uiInterfacing._avatars[_avatarSerialNumber]
        });
    }

    [PunRPC]
	void SendAllPlayerReadyCallBack(){
		PhotonManager_SGDSOFT._photonManager_SGDSOFT._AllPlayerReady = true;
	}
	

	[PunRPC]
	void SendStartGameWave(){
		PhotonManager_SGDSOFT._photonManager_SGDSOFT._StartRace = true;
	}

	void TimeToCallForCountDown(int _timeIndex ){
        
       
		photonView.RPC("TurnningTime",PhotonTargets.AllBuffered,_timeIndex);
	}
	
	[PunRPC]
	void TurnningTime(int _timeIndex){
		GameManager._gameManager._playerIndivisualTimeIndexText.text = "" + _timeIndex;
        GameManager._gameManager._clientTime_ForMultiplayer = _timeIndex;
        /*if(GameManager._gameManager._playerIndivisualTimeIndex <= 0){
            GameManager._gameManager.SwithPlayerManagerButton();
            GameManager._gameManager._playerIndivisualTimeIndex = 40;
        }*/
    }

	//Destory

	public void NetworkPlayerDestory(int destoryObjectID,string _token){         

        photonView.RPC("ServerOtherPlayerDestory",PhotonTargets.AllBuffered,destoryObjectID,_token);

        //Vibrate
        if (PlayerPrefs.GetInt("Vibrate") == 1){
            Handheld.Vibrate();
        }
		
	}
	
	[PunRPC]
	void ServerOtherPlayerDestory(int destoryObjectID,string _token){
		Vector3 _pos = PhotonView.Find(destoryObjectID).gameObject.transform.position;
		Destroy(PhotonView.Find(destoryObjectID).gameObject);
		Instantiate(GameManager._gameManager._destoryEffect,_pos,Quaternion.identity);
		GameManager._gameManager._playerIndivisualTimeIndex = 40;

                    
		for (int i = 0; i < GameManager._gameManager._playerBoard.Count; i++)
        {
            if (GameManager._gameManager._playerBoard[i]._userToken == _token)
            {
                GameManager._gameManager._playerBoard[i]._totalKill++;
            }
        }

		
	}

// Turnning

	public void TurnControll(){
		if(photonView.isMine){
			photonView.RPC("ServerTurnControll",PhotonTargets.AllBuffered);
		}
	}
	
	[PunRPC]
	void ServerTurnControll(){
		GameManager._gameManager._playerIndivisualTimeIndex = 40;
        GameManager._gameManager.SwithPlayerManagerButton();
	}


	public void GameOverLobby(){
		PhotonNetwork.LeaveLobby();
		PhotonNetwork.Disconnect();
	}


	void ServerEmojPushINT(Emoj _Emoj){
		EmojMessage(_Emoj._value);
	}
	//Server Call
	public void EmojMessage(int _indexOfEmoj){
		print(_indexOfEmoj);
		photonView.RPC("EmojServerSend",PhotonTargets.AllBuffered,_isOwner,_indexOfEmoj);
	}


	[PunRPC]
	void EmojServerSend(bool _ower,int _indexOfEmoj){
		if(_ower == true){
			PhotonManager_SGDSOFT._photonManager_SGDSOFT._green_Emoj_Token = PhotonManager_SGDSOFT._photonManager_SGDSOFT._Emojs[_indexOfEmoj];
		}else{
			PhotonManager_SGDSOFT._photonManager_SGDSOFT._blue_Emoj_Token = PhotonManager_SGDSOFT._photonManager_SGDSOFT._Emojs[_indexOfEmoj];
		}
	}

}
