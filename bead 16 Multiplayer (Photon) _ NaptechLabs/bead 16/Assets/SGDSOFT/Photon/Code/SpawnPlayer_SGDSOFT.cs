﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer_SGDSOFT : Photon.MonoBehaviour {
	
	public string _token;
	public int _maxPlayer;
	public string _spawnResourcesName;
	public string _name;
	public bool _isOwnerCall;
	public int _avatarSerialNumber;
	// Use this for initialization
	void OnJoinedRoom () {
			GameObject _customLobby = PhotonNetwork.Instantiate (_spawnResourcesName,transform.position,Quaternion.identity,0);
			_customLobby.GetComponent<PlayerNetworking_SGDSOFT>()._name = _name;
			_customLobby.GetComponent<PlayerNetworking_SGDSOFT>()._isOwner = _isOwnerCall;
			_customLobby.GetComponent<PlayerNetworking_SGDSOFT>()._AvatarSerialNumber = _avatarSerialNumber;
			_customLobby.GetComponent<PlayerNetworking_SGDSOFT>()._maximumJoinningPlayer = _maxPlayer;
			

	}

	
	
}
