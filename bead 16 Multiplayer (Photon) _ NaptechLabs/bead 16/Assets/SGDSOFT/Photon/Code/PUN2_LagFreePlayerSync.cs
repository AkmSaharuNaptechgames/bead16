﻿using UnityEngine;
using Photon;

public class PUN2_LagFreePlayerSync :  PunBehaviour, IPunObservable
{
    //Values that will be synced over network
    Vector3 latestPos;
    Quaternion latestRot;
    //Lag compensation
    float currentTime = 0f;
    double currentPacketTime = 0f;
    double lastPacketTime = 0f;
    Vector3 positionAtLastPacket = Vector3.zero;
    Quaternion rotationAtLastPacket = Quaternion.identity;
    bool _Oneloop_A;

    private Vector3 _PrePos;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            //Network player, receive data
            latestPos = (Vector3)stream.ReceiveNext();
            latestRot = (Quaternion)stream.ReceiveNext();

            //Lag compensation
            currentTime = 0.0f;
            lastPacketTime = currentPacketTime;
            currentPacketTime = info.timestamp;
            positionAtLastPacket = transform.position;
            rotationAtLastPacket = transform.rotation;
        }
    }

    private void Start() {
        _PrePos = this.transform.position;
        GetComponent<PhotonView>().ObservedComponents.Add(this.gameObject.GetComponent<PUN2_LagFreePlayerSync>());
        Invoke("TimeToStart",0.1f);
        this.transform.position = _PrePos;
    }

    void TimeToStart(){
        _Oneloop_A = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine && _Oneloop_A == true)
        {
            //Lag compensation
            double timeToReachGoal = currentPacketTime - lastPacketTime;
            currentTime += Time.deltaTime;

            //Update remote player
            transform.position = latestPos;//Vector3.Lerp(positionAtLastPacket, latestPos, (float)(currentTime / timeToReachGoal));
           // transform.rotation = Quaternion.Lerp(rotationAtLastPacket, latestRot, (float)(currentTime / timeToReachGoal));
        }
    }
}