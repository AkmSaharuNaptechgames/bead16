﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SGDSOFT_JSON.SimpleJSON;
using TMPro;
using System;

public class DatabaseController : MonoBehaviour
{

    public static DatabaseController _DatabaseController {get;set;}
    const string _url = "http://bead16.naptechgames.com/";

    public GameObject _homePanel,_joinPanel,_logPanel,_regPanel;
    public GameObject _userNamePanel_reg,_userEmailPanel_reg,_userAvatarPanel_reg,_userPasswordPanel_reg;
    public GameObject _userEmailPanel_log,_userPasswordPanel_log;
    public TMP_InputField _userName_reg,_userEmail_reg,_userPassword_reg;
    public TMP_InputField _userEmail_log,_userPassword_log;

    [Header("Message")]
    public TextMeshProUGUI _message_of_Login;
    public TextMeshProUGUI _message_of_Registration;

    [Header("DOn't Touch")]
    public int _playerAvatarID;

    
    public GetDataFormLeaderboard _leaderboardData;
    // Start is called before the first frame update
    private void Awake() {
        _DatabaseController = this;
    }
    void Start()
    {
       
        //StartCoroutine(LoginSendData("Ami@gmail.com","123"));
        // StartCoroutine(RegistrationSendData("Ami@gmail.com","Ami","123","0","2"));
        // 

        //   GetDataFormLeaderboard _GetDataFormLeaderboard = new GetDataFormLeaderboard();
        //  _leaderboardData.list.Add(new leaderboardList{user = "test",mail = "test@gmail.com",picid = "1",score = "1"});
        //print(JsonUtility.ToJson(_leaderboardData));


        if (PlayerPrefs.GetString("UserID").ToString().Length > 0){
            _joinPanel.SetActive(false);
            _homePanel.SetActive(true);

        }else{
            _joinPanel.SetActive(true);
            _homePanel.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        //Message Conflect
        
    }

    void MessageNull()
    {
        _message_of_Registration.text = " ";
        _message_of_Login.text = " ";
    }

    public void LogOut(){
        PlayerPrefs.DeleteAll();
    }


    public void LOGIN_Email(){
        //check number
        var _num = _userEmail_log.text;
        if (_num.Length > 0 && _num.Length < 12)
        {
            if (_num[0].ToString() == "0" && _num[1].ToString() == "1"
                && (_num[2].ToString() == "7" || _num[2].ToString() == "8"
                || _num[2].ToString() == "9" || _num[2].ToString() == "6"
                || _num[2].ToString() == "4"))
            {
                _userEmailPanel_log.SetActive(false);
                _userPasswordPanel_log.SetActive(true);
            }
            else
            {
                _message_of_Login.text = "Check your phone number!";
                Invoke("MessageNull", 1f);
            }
        }
        else
        {
            _message_of_Login.text = "Check your phone number!";
            Invoke("MessageNull", 1f);
        }
    }
    public void LOGIN_Pasword(){

        //check number
        var _num = _userPassword_log.text;
        if (_num.Length > 0)
        {           
            Leaderboard._leaderboard._loadingPanel.SetActive(true);
            StartCoroutine(LoginSendData(_userEmail_log.text.ToString(), _userPassword_log.text.ToString()));
        }
        else
        {
            _message_of_Login.text = "Give your own password!";
            Invoke("MessageNull", 1f);
        }
    }
    

    IEnumerator LoginSendData(string _mail,string _pass)
    {
        /*LoginData _data = new LoginData();
         _data.phone = _mail;
         _data.pass = _pass;

         string _json = JsonUtility.ToJson(_data);
         print(_json);*/

        string _url_NEW = _url + "login-api.php?phone=" + _mail + "&pass=" + _pass;

        using (UnityWebRequest www = UnityWebRequest.Get(/*_url+"login-api.php",_json*/_url_NEW))
        {
            //www.method = "Post";
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Leaderboard._leaderboard._loadingPanel.SetActive(false);

                try
                {
                    _message_of_Login.text = "Connection fail!\nTry Again";
                    Invoke("MessageNull", 1f);
                }
                catch (Exception)
                {
                    Debug.LogError("Please Check this line!");
                }
            }
            else
            {
                Debug.Log(www.downloadHandler.text);                       

                JSONNode _node = JSONNode.Parse(www.downloadHandler.text);

                if(_node["status"].ToString() == "false"){
                    _joinPanel.SetActive(true);
                    _regPanel.SetActive(false);
                    _userPasswordPanel_reg.SetActive(false);
                    _userNamePanel_reg.SetActive(true);
                    _homePanel.SetActive(false);
                    Leaderboard._leaderboard._loadingPanel.SetActive(false);

                    try
                    {
                        _message_of_Login.text = "Login fail!";
                        Invoke("MessageNull", 1f);
                    }
                    catch (Exception)
                    {
                        Debug.LogError("Please Check this line!");
                    }

                }else{
                    _joinPanel.SetActive(false);
                    _regPanel.SetActive(false);
                    _userPasswordPanel_reg.SetActive(false);
                    _userNamePanel_reg.SetActive(true);
                    _homePanel.SetActive(true);

                    string _name = _node["user"].Value;
                    int _avatarID = System.Int16.Parse(_node["picid"]);
                    string _uid = _node["uid"].Value;// ToString();
                    string _uscore = _node["score"].Value;// ToString();

                    PlayerPrefs.SetString("UserName",_name);
                    PlayerPrefs.SetInt("AvatarSerialNumber",_avatarID);
                    PlayerPrefs.SetString("UserID",_uid);
                    PlayerPrefs.SetString("UserScore", _uscore);

                    Leaderboard._leaderboard._loadingPanel.SetActive(false);
                }

            }
        }
    }

    [System.Serializable]
    public class LoginData{
        public string phone;
        public string pass;
    }

    public void Reg_Name(){
        if(_userName_reg.text.ToString().Length > 0){
            _userNamePanel_reg.SetActive(false);
            _userEmailPanel_reg.SetActive(true);
        }
    }

    public void Reg_Email(){
        var _num = _userEmail_reg.text.ToString();

        if(_num.Length > 0 && _num.Length < 12)
        {
            if (_num[0].ToString() == "0" && _num[1].ToString() == "1"
                && (_num[2].ToString() == "7" || _num[2].ToString() == "8"
                || _num[2].ToString() == "9" || _num[2].ToString() == "6"
                || _num[2].ToString() == "4"))
            {
                _userEmailPanel_reg.SetActive(false);
                _userAvatarPanel_reg.SetActive(true);
            }
            else
            {
                _message_of_Registration.text = "Check your phone number!";
                Invoke("MessageNull", 1f);
            }
        }
        else
        {
            _message_of_Registration.text = "Check your phone number!";
            Invoke("MessageNull", 1f);
        }
    }

    public void Reg_Password(){
        if(_userPassword_reg.text.ToString().Length > 0){
            Registration();
        }
    }

    void Registration(){
        Leaderboard._leaderboard._loadingPanel.SetActive(true);
        StartCoroutine(RegistrationSendData(_userEmail_reg.text.ToString(),
        _userName_reg.text.ToString(),_userPassword_reg.text.ToString(),"0",_playerAvatarID.ToString()));
    }

    IEnumerator RegistrationSendData(string _phone,string _user,string _pass,string _score,string _picid)
    {
        /* RegistrationData _data = new RegistrationData();
         _data.phone = _phone;
         _data.user = _user;
         // _data.mail = _mail;       
         _data.pass = _pass;
         _data.score = _score;
         _data.picid = _picid;


         string _json = JsonUtility.ToJson(_data);
         print(_json);*/

        WWWForm _from = new WWWForm();
        _from.AddField("phone", "123");
        _from.AddField("user", "AAA");
        _from.AddField("pass", "6745");
        _from.AddField("score", "1");
        _from.AddField("picid", "1");
        string _url_NEW = _url+"register-api.php?phone=" + _phone + "&user=" + _user + "&pass=" + _pass + "&score=1&picid="
            + _picid;

        using (UnityWebRequest www = UnityWebRequest.Get(/*_url+"register-api.php"*/_url_NEW))
        {
            www.method = "Get";
            yield return www.SendWebRequest();
            

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string _json_data = www.downloadHandler.text;

                JSONNode _node = JSONNode.Parse(_json_data);

                if (_node["status"].ToString() == "true")
                {
                    _joinPanel.SetActive(true);
                    _regPanel.SetActive(false);
                    _userPasswordPanel_reg.SetActive(false);
                    _userNamePanel_reg.SetActive(true);
                    Leaderboard._leaderboard._loadingPanel.SetActive(false);
                }
                else
                {
                    _message_of_Registration.text = "Already Have Account!"; 
                    Leaderboard._leaderboard._loadingPanel.SetActive(false);
                    Invoke("MessageNull", 1f);
                }
            }
        }
    }

     [System.Serializable]
    public class RegistrationData{
        
        public string user;
        //  public string mail;
        public string phone;
        public string pass;
        public string score;
        public string picid;
        
    }

   

    public void ShowLeaderboard(){
        Leaderboard._leaderboard._loadingPanel.SetActive(true);
         StartCoroutine(LeaderboardGetData(PlayerPrefs.GetString("UserID").ToString()));
    }

    IEnumerator LeaderboardGetData(string _userId)
    {
       /* leaderboardData _data = new leaderboardData();
        _data.uid = _userId;

        string _json = JsonUtility.ToJson(_data);
        print(_json);*/

        using (UnityWebRequest www = UnityWebRequest.Get(_url+"leaderboard-api.php"/*,_json*/))
        {
           // www.method = "Post";
            yield return www.SendWebRequest();
            

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string _resultJson = www.downloadHandler.text;
               _leaderboardData = JsonUtility.FromJson<GetDataFormLeaderboard>(_resultJson);
               if(_leaderboardData.list.Count > 0){
                   for(int i = 0; i < _leaderboardData.list.Count; i++){
                       int _picID = 0;
                       if(_leaderboardData.list[i].picid == "0"){
                           _picID = 0;
                       }else if(_leaderboardData.list[i].picid == "1"){
                           _picID = 1;
                       }else if(_leaderboardData.list[i].picid == "2"){
                           _picID = 2;
                       }else if(_leaderboardData.list[i].picid == "3"){
                           _picID = 3;
                       }
                       Leaderboard._leaderboard.PlayerInfoMake(_leaderboardData.list[i].user,i,_leaderboardData.list[i].score,_picID);
                   }
               }

                Leaderboard._leaderboard._loadingPanel.SetActive(false);
            }
        }
    }

     [System.Serializable]
    public class leaderboardData{
        public string uid;
    
    }

       [System.Serializable]
    public class GetDataFormLeaderboard{
        public List<leaderboardList> list;
    }

    [System.Serializable]
    public class leaderboardList{
        public string user;
        public string mail;
        public string score;
        public string picid;
    }


}
