﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Leaderboard : MonoBehaviour
{
    public static Leaderboard _leaderboard {get;set;}
    public Transform _content;
    public GameObject _infoPrefab;
    public GameObject _loadingPanel;

    [Header("User INFO")]
    public Image _pic;
    public TextMeshProUGUI _name;
    public TextMeshProUGUI _score;

    [Header("Don't Touch Here")]
    public List<GameObject> _playerINFOLists;
    

    //[Header("UI")]

    private void Awake() {
        _leaderboard = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _name.text = "" + PlayerPrefs.GetString("UserName");
        _pic.sprite = MainMenuController._MainMenuController._avaters[PlayerPrefs.GetInt("AvatarSerialNumber")];
        _score.text = "Score : " + PlayerPrefs.GetString("UserScore");
    }

    public void PlayerInfoMake(string _name,int serial,string score,int _picID){
        GameObject _player = Instantiate(_infoPrefab,transform.position,Quaternion.identity);
        _player.name = _name;
        _player.transform.SetParent(_content);
        _player.transform.localScale = new Vector3(1,1,1);

        _player.GetComponent<LeaderboardPlayerInfo>()._name.text = _name;
        _player.GetComponent<LeaderboardPlayerInfo>()._serial.text = "" + serial;
        _player.GetComponent<LeaderboardPlayerInfo>()._score.text = "" + score;
        _player.GetComponent<LeaderboardPlayerInfo>()._pic.sprite = MainMenuController._MainMenuController._avaters[_picID];

        _playerINFOLists.Add(_player);

        
    }

    public void ClearList(){
        _playerINFOLists.Clear();
        DatabaseController._DatabaseController._leaderboardData.list.Clear();
    }

}
