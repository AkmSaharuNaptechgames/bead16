﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SGDSOFT_JSON.SimpleJSON;
using TMPro;
using System;

public class UpdateDatabase : MonoBehaviour
{

    public static UpdateDatabase _UpdateDatabase { get; set; }

    const string _url = "http://bead16.naptechgames.com/";
    public GameObject _Loading_Panel_Exit_Save_Database_Upadate;
    [Header("Don't Touch Here")]
    public int _totalKills_Update;
    void Awake()
    {
        _UpdateDatabase = this;
    }

    void Start()
    {
    
        _Loading_Panel_Exit_Save_Database_Upadate.SetActive(false);
       
    }

    public void UpdateSCORE()
    {
        _Loading_Panel_Exit_Save_Database_Upadate.SetActive(true);
        int _scorePRE = System.Int16.Parse(PlayerPrefs.GetString("UserScore"));
        int _updateScore = _totalKills_Update + _scorePRE;
        PlayerPrefs.SetString("UserScore", "" + _updateScore);
        StartCoroutine(UpdateScoreSendData(PlayerPrefs.GetString("UserID"), "" + _updateScore));
    }

    IEnumerator UpdateScoreSendData(string _userId, string _score)
    {
        ScoreUpdateData _data = new ScoreUpdateData();
        _data.uid = _userId;
        _data.score = _score;

        string _json = JsonUtility.ToJson(_data);
        print(_json);

        using (UnityWebRequest www = UnityWebRequest.Put(_url + "score-update-api.php", _json))
        {
            www.method = "Post";
            yield return www.SendWebRequest();


            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                PhotonManager_SGDSOFT._photonManager_SGDSOFT.ExitLobby(1);
            }
        }
    }

    [System.Serializable]
    public class ScoreUpdateData
    {
        public string uid;
        public string score;


    }

}
