﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class LeaderboardPlayerInfo : MonoBehaviour
{
    
    public TextMeshProUGUI _serial;
    public TextMeshProUGUI _name;
    public TextMeshProUGUI _score;
    public Image _pic;

    
}
